package gui;

import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import logic.ConnectionLogic;

public class TabConnection extends GridPane {
	private TextField storage, userName;
	private PasswordField passWord;
	private Alert alert = new Alert(AlertType.INFORMATION);
	private MainApp app;
	private Button btnConnect;
	
	public TabConnection(MainApp app) {
		this.app = app;
		setPadding(new Insets(20));
		setVgap(10);
		setHgap(10);
		
		initLogInContent();
	}
	
	private void initLogInContent() {
		VBox logInBox = new VBox();
		
		// Which database is needed
		logInBox.getChildren().add(new Label("Storage"));
		storage = new TextField("LAPTOP-OL1V289M");
		logInBox.getChildren().add(storage);
				
		// Which user is logging in
		logInBox.getChildren().add(new Label("Username:"));
		userName = new TextField("sa");
		logInBox.getChildren().add(userName);
				
		// Specific password for the user
		logInBox.getChildren().add(new Label("Password:"));
		passWord = new PasswordField();
		logInBox.getChildren().add(passWord);
		
		// Connect button
		btnConnect = new Button("Login");
		btnConnect.setOnAction(event -> loginAction());
		logInBox.getChildren().add(btnConnect);
		
		// Use css-like styling for border
		logInBox.setStyle(
				"-fx-padding: 10;" +
                "-fx-border-style: solid;");
		add(logInBox, 0, 0);
	}
	
	private void loginAction() {
		ConnectionLogic cl = ConnectionLogic.connectionLogic();
		try {
			cl.connectUser(storage.getText().trim(), userName.getText().trim(), passWord.getText().trim());
			app.logedIn();
		} catch (Exception e) {
			alert.setTitle("Connection error");
			alert.setHeaderText("Something went wrong while connection");
			alert.setContentText(e.getMessage());
			alert.showAndWait();
		}
	}
}