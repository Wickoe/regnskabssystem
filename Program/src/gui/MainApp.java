package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.stage.Stage;
import logic.ConnectionLogic;

public class MainApp extends Application {
	private TabPane tabPane;
	
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Regnskabssystem");

		initContent();
		
		for(int i = 1; i < tabPane.getTabs().size(); i++) {
			tabPane.getTabs().get(i).setDisable(true);
		}
		
		Scene scene = new Scene(tabPane);
		stage.setScene(scene);
		stage.show();
	}
	
	private void initContent() {
		tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tab = new Tab("Login");
		tab.setContent(new TabConnection(this));
		tabPane.getTabs().add(tab);
		
		tab = new Tab("Oversigt");
		tab.setContent(new TabOversigt());
		tabPane.getTabs().add(tab);
	}
	
	public void logedIn() {
		tabPane.getTabs().get(0).setDisable(true);
		tabPane.getSelectionModel().select(1);
		for(int i = 1; i < tabPane.getTabs().size(); i++) {
			tabPane.getTabs().get(i).setDisable(false);
		}
	}
	
	@Override
	public void stop() {
		ConnectionLogic lc = ConnectionLogic.connectionLogic();
		
		try {
			lc.disconnectUser();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
		}
	}
}
