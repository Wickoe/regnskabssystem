package logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;

import storageConnection.StorageConnection;

public class ConnectionLogic extends Thread {
	private static ConnectionLogic cl = null;
	private Connection con;
	
	private ConnectionLogic() {
		// TODO
	}
	
	public static ConnectionLogic connectionLogic() {
		if(cl == null) {
			cl = new ConnectionLogic();
		}
		return cl;
	}
	
	public void connectUser(String database, String userName, String passWord) throws SQLTimeoutException, SQLException, Exception {
		if(database != null && userName != null && passWord != null) {
			con = DriverManager.getConnection("jdbc:sqlserver://" + database + ";databaseName=Regnskabssystem;user=" + userName + ";password=" + passWord);
			StorageConnection sc = StorageConnection.getStorageConnection();
			sc.setConnection(con);
		} else {
			throw new Exception("Missing username and/or password");
		}
	}
	
	public void disconnectUser() throws SQLException {
		if(con != null) {
			con.close();
		}
	}
}
