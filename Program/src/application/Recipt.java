package application;

import java.util.ArrayList;
import java.util.List;

public class Recipt {
	private Butik butik;
	private List<Vare> varer;
	
	public Recipt(Butik enButik) {
		this.butik = enButik;
		varer = new ArrayList<>();
	}

	public Butik getButik() {
		return butik;
	}

	public void setButik(Butik butik) {
		this.butik = butik;
	}

	public List<Vare> getVarer() {
		return new ArrayList<>(varer);
	}

	public void setVarer(List<Vare> varer) {
		this.varer = varer;
	}
	
	
	public void addVare(Vare enVare) {
		varer.add(enVare);
	}
}
