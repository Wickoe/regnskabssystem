package application;

public class Butik {
	private String navn;
	private Adresse adresse;
	
	public Butik(String etNavn, Adresse enAdresse) {
		this.navn = etNavn;
		this.adresse = enAdresse;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
}
