package application;

public class Adresse {
	private String vej;
	private String by;
	private String postnummer;
	
	public Adresse(String enVej, String enBy, String etPostNummer) {
		this.vej = enVej;
		this.by = enBy;
		this.postnummer = etPostNummer;
	}

	public String getVej() {
		return vej;
	}

	public void setVej(String vej) {
		this.vej = vej;
	}

	public String getBy() {
		return by;
	}

	public void setBy(String by) {
		this.by = by;
	}

	public String getPostnummer() {
		return postnummer;
	}

	public void setPostnummer(String postnummer) {
		this.postnummer = postnummer;
	}
}
