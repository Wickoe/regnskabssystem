package application;

public class Vare {
	private String beskrivelse;
	private boolean eco;
	private double pris;
	
	public Vare(String enBeskrivelse, boolean isEco, double enPris) {
		this.beskrivelse = enBeskrivelse;
		this.eco = isEco;
		this.pris = enPris;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public boolean isEco() {
		return eco;
	}

	public void setEco(boolean eco) {
		this.eco = eco;
	}

	public double getPris() {
		return pris;
	}

	public void setPris(double pris) {
		this.pris = pris;
	}
}
