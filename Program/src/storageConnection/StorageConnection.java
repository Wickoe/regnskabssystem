package storageConnection;

import java.sql.Connection;

public class StorageConnection {
	private static StorageConnection sc;
	private Connection con;
	
	private StorageConnection() {
		// TODO
	}
	
	public static StorageConnection getStorageConnection() {
		if(sc == null) {
			sc = new StorageConnection();
		}
		return sc;
	}
	
	public void setConnection(Connection con) {
		this.con = con;
	}

	public Connection getConnection() {
		return con;
	}
}
